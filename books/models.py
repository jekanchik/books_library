from django.db import models

# Create your models here.


class Book(models.Model):
    upload_file = models.FileField(upload_to='uploads/')
    date_created = models.DateTimeField(auto_now=True)
    name = models.CharField(max_length=255)


class Searching(models.Model):
    email = models.EmailField()
    search_string = models.CharField(max_length=255)
    date_created = models.DateTimeField(auto_now=True, null=True)
