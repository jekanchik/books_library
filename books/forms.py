# -*- coding: utf-8 -*-

from django.forms import ModelForm

from books.models import Searching


class SearchingForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(SearchingForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'required': True,
                'class': 'form-control'
            })
            if field == 'email':
                self.fields[field].widget.attrs.update({'placeholder': 'Your e-mail for search results',})
            elif field == 'search_string':
                self.fields[field].widget.attrs.update({'placeholder': 'What we will search in books?',})

    class Meta:
        model = Searching
        fields = ('search_string', 'email')