from django.contrib import admin
from books.models import Book, Searching

# Register your models here.
class BooksAdmin(admin.ModelAdmin):
    list_display = ('name', 'upload_file', 'date_created')
    fieldsets = (
        (None, {'fields': ('name', 'upload_file',)}),
    )


class SearchingAdmin(admin.ModelAdmin):
    list_display = ('email', 'search_string', 'date_created')
    fieldsets = (
        (None, {'fields': ('email', 'search_string')}),
    )


admin.site.register(Book, BooksAdmin)
admin.site.register(Searching, SearchingAdmin)