# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse
from django.shortcuts import render, redirect

from PyPDF2 import PdfFileReader, PdfFileWriter

from books.lib import convert_pdf
from books.forms import SearchingForm
from books.models import  Searching, Book

# Create your views here.


def index(request):
    form = SearchingForm()
    if request.method == 'POST':
        form = SearchingForm(request.POST)
        if form.is_valid():
            search_values = form.save()
            return redirect(reverse('search_result', kwargs={'pk': search_values.pk }))
            
        else:
            print('INVALID')
    return render(request,
                'index.html',
                {
                    'form': form
                })

def allindices(string, sub, listindex=[], offset=0):
    i = string.find(sub, offset)
    while i >= 0:
        listindex.append(i)
        i = string.find(sub, i + 1)
    return listindex

def search_result(request, pk):
    search_values = Searching.objects.get(pk=pk)
    book = Book.objects.first()
    result = []
    book_text = PdfFileReader(open(book.upload_file.path, "rb"))
    for page in range(1, book_text.getNumPages()):
        book_page = book_text.getPage(page)
        res = book_page.extractText().find(search_values.search_string)
        if res:
            print(book_page.extractText())
            result.append(book_page.extractText())
    return render(request,
            'search_result.html',
            {
                'search_values': search_values,
                'result': (result if result else None)
            }
            )
