
from django.conf.urls import url
from django.contrib import admin
from books.views import index, search_result


urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'search_result/(?P<pk>[0-9]+)$', search_result, name='search_result'),
    
]